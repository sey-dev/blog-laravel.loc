## install project
docker-compose up -d --build

## Seeder for models
php artisan db:seed --class=ArticleSeeder
php artisan db:seed --class=CategorySeeder
php artisan db:seed --class=MenuSeeder

## Unit tests
php artisan test

## Generate Api documentation
php artisan l5-swagger:generate
