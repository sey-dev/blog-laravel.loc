<?php

namespace Tests\Unit\Controllers\Api;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_method_returns_all_categories()
    {
        Category::factory()->count(3)->create();

        $response = $this->getJson('/api/categories');

        $response->assertStatus(200)
            ->assertJsonCount(3);
    }

    public function test_show_method_returns_specific_category()
    {
        $category = Category::factory()->create();

        $response = $this->getJson("/api/categories/{$category->id}");

        $response->assertStatus(200)
            ->assertJson([
                    'id' => $category->id,
            ]);
    }

    public function test_store_method_creates_new_category()
    {
        $data = [
            'name' => 'Test Category',
            // Додайте інші дані, які ви хочете передати через запит
        ];

        $response = $this->postJson('/api/categories', $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJson($data);

        $this->assertDatabaseHas('categories', $data);
    }

    public function test_update_method_updates_category()
    {
        $category = Category::factory()->create();
        $newData = [
            'name' => 'Updated Category Name',
        ];

        $response = $this->putJson("/api/categories/{$category->id}", $newData);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson($newData);

        $this->assertDatabaseHas('categories', $newData);
    }

    public function test_destroy_method_deletes_category()
    {
        $category = Category::factory()->create();

        $response = $this->deleteJson("/api/categories/{$category->id}");

        $response->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('categories', ['id' => $category->id]);
    }
}
