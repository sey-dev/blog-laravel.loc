<?php

namespace Tests\Unit\Controllers\Api;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ArticleControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_method_returns_all_articles()
    {
        Article::factory()->count(3)->create();

        $response = $this->getJson('/api/articles');

        $response->assertStatus(200)
            ->assertJsonCount(3);
    }

    public function test_show_method_returns_specific_article()
    {
        $article = Article::factory()->create();

        $response = $this->getJson("/api/articles/{$article->id}");

        $response->assertStatus(200)
            ->assertJson([
                'id' => $article->id,
                "title" => $article->title,
                "content" => $article->content,
            ]);
    }

    public function test_store_method_creates_new_article_with_valid_data()
    {
        $category = Category::factory()->create();
        $data = [
            'title' => 'Test Article',
            'content' => 'Lorem ipsum dolor sit amet.',
            'category_id' => $category->id,
        ];

        $response = $this->postJson('/api/articles', $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJson([
                'title' => 'Test Article',
                'content' => 'Lorem ipsum dolor sit amet.',
            ]);
    }

    public function test_update_method_updates_existing_article_with_valid_data()
    {
        $article = Article::factory()->create();
        $category = Category::factory()->create();
        $data = [
            'title' => 'Updated Article Title',
            'content' => 'Updated content',
            'category_id' => $category->id,
        ];

        $response = $this->putJson("/api/articles/{$article->id}", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'title' => 'Updated Article Title',
                'content' => 'Updated content',
            ]);
    }

    public function test_destroy_method_deletes_existing_article()
    {
        $article = Article::factory()->create();

        $response = $this->deleteJson("/api/articles/{$article->id}");

        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertDatabaseMissing('articles', ['id' => $article->id]);
    }
}
