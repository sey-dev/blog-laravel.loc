<?php

namespace Tests\Unit\Controllers\Api;

use App\Models\Article;
use App\Models\Menu;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class MenuControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_method_returns_all_menus()
    {
        Menu::factory()->count(3)->create();

        $response = $this->getJson('/api/menus');

        $response->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(3);
    }

    public function test_show_method_returns_specific_menu()
    {
        $menu = Menu::factory()->create();

        $response = $this->getJson("/api/menus/{$menu->id}");

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'id' => $menu->id,
                'name' => $menu->name,
            ]);
    }

    public function test_store_method_creates_new_menu_with_valid_data()
    {
        $article = Article::factory()->create();
        $data = [
            'name' => 'Test Menu',
            'article_id' => $article->id,
        ];

        $response = $this->postJson('/api/menus', $data);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJson([
                'name' => 'Test Menu',
            ]);
    }

    public function test_update_method_updates_existing_menu_with_valid_data()
    {
        $menu = Menu::factory()->create();
        $article = Article::factory()->create();
        $data = [
            'name' => 'Updated Menu Name',
            'article_id' => $article->id,
        ];

        $response = $this->putJson("/api/menus/{$menu->id}", $data);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'name' => 'Updated Menu Name',
            ]);
    }

    public function test_destroy_method_deletes_existing_menu()
    {
        $menu = Menu::factory()->create();

        $response = $this->deleteJson("/api/menus/{$menu->id}");

        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertDatabaseMissing('menus', ['id' => $menu->id]);
    }
}
