<?php

namespace App\Constants;

class BlogConstants
{
    public const ARTICLE = 'article';
    public const MENU = 'menu';
    public const CATEGORY = 'category';
}
