<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [
            "id" => $this->id,
            "name" => $this->name,
        ];

        $menus = $this->menus;

        if ($menus->isNotEmpty()) {
            $menuResources = MenuResource::collection($menus);
            $data['menus'] = $menuResources;
        }

        return $data;
    }
}
