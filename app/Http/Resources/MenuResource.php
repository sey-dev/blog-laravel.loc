<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [
            "id" => $this->id,
            "name" => $this->name,
        ];

        $articles = $this->articles;

        if ($articles->isNotEmpty()) {
            $articleResources = ArticleResource::collection($articles);
            $data['articles'] = $articleResources;
        }

        return $data;
    }
}
