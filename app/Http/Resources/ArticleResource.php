<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [
            "id" => $this->id,
            "title" => $this->title,
            "content" => $this->content,
        ];

        $categories = $this->categories;

        if ($categories->isNotEmpty()) {
            $categoryResources = CategoryResource::collection($categories);
            $data['categories'] = $categoryResources;
        }

        return $data;
    }
}
