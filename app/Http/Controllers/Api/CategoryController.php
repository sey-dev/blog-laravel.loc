<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Constants\BlogConstants;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Service\ExceptionHandlerService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    /**
     * @OA\Get(
     *     path="/categories",
     *     summary="Get a list of categories",
     *     tags={"Categories"},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(
     *                      property="menus",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string")
     *                      )
     *                   )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid request"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function index(): Response
    {
        try {
            return response(CategoryResource::collection(Category::all()));
        } catch (\Exception $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::CATEGORY);
        }
    }

    /**
     * @OA\Get(
     *     path="/categories{id}",
     *     summary="Get a list of categories",
     *     tags={"Categories"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the category",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(
     *                      property="menus",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string")
     *                      )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid request"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function show($id): Response
    {
        try {
            return response(new CategoryResource(Category::findOrFail($id)));
        } catch (ModelNotFoundException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::CATEGORY);
        }
    }

    /**
     * @OA\Post(
     *     path="/categories",
     *     summary="Create a new category",
     *     tags={"Categories"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Category data",
     *         @OA\JsonContent(
     *             required={"name"},
     *             @OA\Property(property="name", type="string", example="Name content"),
     *             @OA\Property(property="menu_id", type="integer", example=19, nullable=true)
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(
     *                      property="menus",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string")
     *                      )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid data provided"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function store(CategoryRequest $request): Response
    {
        try {
            $category = Category::create($request->validated());

            if ($request->has('menu_id')) {
                $category->menus()->attach($request->menu_id);
            }

            return response(new CategoryResource($category), Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::CATEGORY);
        }
    }

    /**
     * @OA\Put(
     *     path="/categories",
     *     summary="Create a new category",
     *     tags={"Categories"},
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="ID of the article",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Category data",
     *         @OA\JsonContent(
     *             required={"name"},
     *             @OA\Property(property="name", type="string", example="Name content"),
     *             @OA\Property(property="menu_id", type="integer", example=19, nullable=true)
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(
     *                      property="menus",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string")
     *                      )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid data provided"),
     *     @OA\Response(response=404, description="Category not found"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function update(CategoryRequest $request, $id): Response
    {
        try {
            $category = new CategoryResource(Category::findOrFail($id));
            $category->update($request->validated());

            if ($request->has('menu_id')) {
                $category->menus()->sync([$request->menu_id]);
            }

            return response($category, Response::HTTP_OK);
        } catch (ModelNotFoundException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::CATEGORY);
        }
    }

    /**
     * @OA\Delete(
     *     path="/categories/{id}",
     *     summary="Delete an category by ID",
     *     tags={"Categories"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the category to delete",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(response=204, description="Category deleted"),
     *     @OA\Response(response=404, description="Category not found"),
     *     @OA\Response(response=400, description="Invalid data provided"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function destroy($id): Response
    {
        try {
            Category::findOrFail($id)->delete();

            return response(null, Response::HTTP_NO_CONTENT);
        } catch (ModelNotFoundException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::CATEGORY);
        }
    }
}
