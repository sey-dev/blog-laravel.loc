<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Constants\BlogConstants;
use App\Http\Controllers\Controller;
use App\Http\Requests\MenuRequest;
use App\Http\Resources\MenuResource;
use App\Models\Menu;
use App\Service\ExceptionHandlerService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends Controller
{
    /**
     * @OA\Get(
     *     path="/menus",
     *     summary="Get a list of menus",
     *     tags={"Menus"},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(
     *                      property="articles",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="title", type="string"),
     *                          @OA\Property(property="content", type="string"),
     *                      )
     *                   )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid request"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function index(): Response
    {
        try {
            return response(MenuResource::collection(Menu::all()));
        } catch (\Exception $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::MENU);
        }
    }

    /**
     * @OA\Get(
     *     path="/menus{id}",
     *     summary="Get a list of menus",
     *     tags={"Menus"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the menu",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(
     *                      property="categories",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="title", type="string"),
     *                          @OA\Property(property="content", type="string"),
     *                      )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid request"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function show($id): Response
    {
        try {
            return response(new MenuResource(Menu::findOrFail($id)));
        } catch (ModelNotFoundException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::MENU);
        }
    }

    /**
     * @OA\Post(
     *     path="/menus",
     *     summary="Create a new menu",
     *     tags={"Menus"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Menu data",
     *         @OA\JsonContent(
     *             required={"title", "content", "article_id"},
     *             @OA\Property(property="name", type="string", example="Sample name"),
     *             @OA\Property(property="article_id", type="integer", example=19, nullable=true)
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(
     *                      property="categories",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="title", type="string"),
     *                          @OA\Property(property="content", type="string"),
     *                      )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid data provided"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function store(MenuRequest $request): Response
    {
        try {
            $menu = Menu::create($request->validated());

            if ($request->has('article_id')) {
                $menu->articles()->attach($request->article_id);
            }

            return response(new MenuResource($menu), Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::MENU);
        }
    }

    /**
     * @OA\Put(
     *     path="/menus",
     *     summary="Create a new menu",
     *     tags={"Menus"},
     *     @OA\Parameter(
     *          name="id",
     *          in="path",
     *          required=true,
     *          description="ID of the menu",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Menu data",
     *         @OA\JsonContent(
     *             required={"title", "content", "article_id"},
     *             @OA\Property(property="name", type="string", example="Sample name"),
     *             @OA\Property(property="article_id", type="integer", example=19, nullable=true)
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="name", type="string"),
     *                  @OA\Property(
     *                      property="categories",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="title", type="string"),
     *                          @OA\Property(property="content", type="string"),
     *                      )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid data provided"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function update(MenuRequest $request, $id): Response
    {
        try {
            $menu = new MenuResource(Menu::findOrFail($id));
            $menu->update($request->validated());


            if ($request->has('article_id')) {
                $menu->articles()->sync([$request->article_id]);
            }

            return response($menu, Response::HTTP_OK);
        } catch (ModelNotFoundException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::MENU);
        }
    }

    /**
     * @OA\Delete(
     *     path="/menus/{id}",
     *     summary="Delete an menu by ID",
     *     tags={"Menus"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the menu to delete",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(response=204, description="Menu deleted"),
     *     @OA\Response(response=404, description="Menu not found"),
     *     @OA\Response(response=400, description="Invalid data provided"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function destroy($id): Response
    {
        try {
            Menu::findOrFail($id)->delete();

            return response(null, Response::HTTP_NO_CONTENT);
        } catch (ModelNotFoundException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::MENU);
        }
    }
}
