<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Constants\BlogConstants;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Service\ExceptionHandlerService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

class ArticleController extends Controller
{
    /**
     * @OA\Get(
     *     path="/articles",
     *     summary="Get a list of articles",
     *     tags={"Articles"},
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="content", type="string"),
     *                  @OA\Property(
     *                      property="categories",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string")
     *                      )
     *                   )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid request"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function index(): Response
    {
        try {
            return response(ArticleResource::collection(Article::all()));
        } catch (\Exception $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::ARTICLE);
        }
    }

    /**
     * @OA\Get(
     *     path="/articles{id}",
     *     summary="Get a list of articles",
     *     tags={"Articles"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the article",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="content", type="string"),
     *                  @OA\Property(
     *                      property="categories",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string")
     *                      )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid request"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function show($id): Response
    {
        try {
            return response(new ArticleResource(Article::findOrFail($id)));
        } catch (ModelNotFoundException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::ARTICLE);
        }
    }

    /**
     * @OA\Post(
     *     path="/articles",
     *     summary="Create a new article",
     *     tags={"Articles"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Article data",
     *         @OA\JsonContent(
     *             required={"title", "content", "category_id"},
     *             @OA\Property(property="title", type="string", example="Sample title"),
     *             @OA\Property(property="content", type="string", example="Sample content"),
     *             @OA\Property(property="category_id", type="integer", example=19, nullable=true)
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="content", type="string"),
     *                  @OA\Property(
     *                      property="categories",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string")
     *                      )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=400, description="Invalid data provided"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function store(ArticleRequest $request): Response
    {
        try {
            $article = Article::create($request->validated());

            if ($request->has('category_id')) {
                $article->categories()->attach($request->category_id);
            }

            return response(new ArticleResource($article), Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::ARTICLE);
        }
    }

    /**
     * @OA\Put(
     *     path="/articles",
     *     summary="Create a new article",
     *     tags={"Articles"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the article",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Article data",
     *         @OA\JsonContent(
     *             required={"title", "content", "category_id"},
     *             @OA\Property(property="title", type="string", example="Sample title"),
     *             @OA\Property(property="content", type="string", example="Sample content"),
     *             @OA\Property(property="category_id", type="integer", example=19, nullable=true)
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful operation",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                  @OA\Property(property="id", type="integer"),
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="content", type="string"),
     *                  @OA\Property(
     *                      property="categories",
     *                      type="array",
     *                      @OA\Items(
     *                          @OA\Property(property="id", type="integer"),
     *                          @OA\Property(property="name", type="string")
     *                      )
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(response=404, description="Article not found"),
     *     @OA\Response(response=400, description="Invalid data provided"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function update(ArticleRequest $request, $id): Response
    {
        try {
            $article = new ArticleResource(Article::findOrFail($id));
            $article->update($request->validated());

            if ($request->has('category_id')) {
                $article->categories()->sync([$request->category_id]);
            }

            return response($article, Response::HTTP_OK);
        } catch (ModelNotFoundException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::ARTICLE);
        }
    }

    /**
     * @OA\Delete(
     *     path="/articles/{id}",
     *     summary="Delete an article by ID",
     *     tags={"Articles"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the article to delete",
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(response=204, description="Article deleted"),
     *     @OA\Response(response=404, description="Article not found"),
     *     @OA\Response(response=400, description="Invalid data provided"),
     *     @OA\Response(response=422, description="Unprocessable Entity"),
     *     @OA\Response(response=500, description="Internal Server Error")
     * )
     */
    public function destroy($id): Response
    {
        try {
            Article::findOrFail($id)->delete();

            return response(null, Response::HTTP_NO_CONTENT);
        } catch (ModelNotFoundException $e) {
            return ExceptionHandlerService::handle($e, BlogConstants::ARTICLE);
        }
    }
}
