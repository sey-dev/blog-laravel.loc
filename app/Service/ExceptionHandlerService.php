<?php

declare(strict_types=1);

namespace App\Service;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpFoundation\Response;

class ExceptionHandlerService
{
    public static function handle(\Exception $e, $blogName): Response
    {
        $notFoundMessage = ucfirst($blogName) . ' not found';

        if ($e instanceof ModelNotFoundException) {
            return response(['message' => $notFoundMessage], Response::HTTP_NOT_FOUND);
        }

        if ($e instanceof QueryException) {
            return response(['message' => 'Invalid data provided'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response(['message' => 'Server error'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
